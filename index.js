const express = require('express')
const Axios = require('axios')
const bodyParser = require('body-parser')
const conf = require('./server/config')
require('./server/buffer')


const app = express()
app.use(bodyParser.json())
app.use(express.static(__dirname + '/client'))

app.use('/api/forge/oauth', require('./server/oauth').router)
app.use('/api/forge/datamanagement/bucket', require('./server/bucket').router)
app.use('/api/forge/modelderivative', require('./server/translate').router)

app.set('port', conf.server.port)
const server = app.listen(app.get('port'), function () {
    console.log('Server listening on port ' + server.address().port)
})
