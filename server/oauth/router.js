const express = require('express')
const router = new express.Router()
const handler = require('./handler')

router.get('/token/private', handler.private)
router.get('/token/public', handler.public)

module.exports = router
