const querystring = require('querystring')
const Axios = require('axios').default
const conf = require('../config')

const private = async (_req, res) => {
    try {
        const response = await authenWithForge('data:read data:write data:create bucket:create bucket:read')
        global.access_token = response.data.access_token
        res.redirect('/api/forge/datamanagement/bucket/create')
    } catch (error) {
        console.error(error)
        res.send('Failed to authenticate')
    }
}

const public = async (_req, res) => {
    try {
        const response = await authenWithForge('viewables:read')
        res.json({ access_token: response.data.access_token, expires_in: response.data.expires_in })
    } catch (error) {
        console.error(error)
        res.status(500).json(error)
    }
}

const authenWithForge = async (scope) => {
    return await Axios({
        method: 'POST',
        url: 'https://developer.api.autodesk.com/authentication/v1/authenticate',
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
        },
        data: querystring.stringify({
            client_id: conf.forge.clientID,
            client_secret: conf.forge.clientSecret,
            grant_type: 'client_credentials',
            scope: scope,
        })
    })
}

module.exports = {private, public}
