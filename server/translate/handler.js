const Axios = require('axios').default
const conf = require('../config')
const fs = require('fs').promises

const translate = async (req, res) => {
    try {
        var urn = req.params.urn
        var format_type = 'svf'
        var format_views = ['2d', '3d']
        await Axios({
            method: 'POST',
            url: 'https://developer.api.autodesk.com/modelderivative/v2/designdata/job',
            headers: {
                'content-type': 'application/json',
                Authorization: 'Bearer ' + global.access_token
            },
            data: JSON.stringify({
                'input': {
                    'urn': urn
                },
                'output': {
                    'formats': [
                        {
                            'type': format_type,
                            'views': format_views
                        }
                    ]
                }
            })
        })
        res.redirect('/viewer.html?urn=' + urn)
    } catch (error) {
        console.error(error)
        res.send('Error at Model Derivative job.')
    }
}

module.exports = { translate }
