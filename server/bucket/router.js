const express = require('express')
const router = new express.Router()
const handler = require('./handler')
const conf = require('../config')
var multer = require('multer')
var upload = multer({ dest: `${conf.tempFolder}/` })


router.get('/create', handler.create)
router.get('/detail', handler.detail)
router.post('/upload', upload.single('fileToUpload'), handler.upload)

module.exports = router
