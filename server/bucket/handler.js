const Axios = require('axios').default
const conf = require('../config')
const fs = require('fs').promises
const now = new Date().getTime()
const bucketKey = `${conf.forge.clientID.toLowerCase()}_${now}` // to make it unique

const create = async (_req, res) => {
    try {
        await Axios({
            method: 'POST',
            url: 'https://developer.api.autodesk.com/oss/v2/buckets',
            headers: {
                'content-type': 'application/json',
                Authorization: 'Bearer ' + global.access_token
            },
            data: JSON.stringify({
                'bucketKey': bucketKey,
                'policyKey': conf.forge.policyKey,
            })
        })

        res.redirect('/api/forge/datamanagement/bucket/detail')
    } catch (error) {
        if (error.response && error.response.status == 409) {
            console.log('Bucket already exists, skip creation.')
            res.redirect('/api/forge/datamanagement/bucket/detail')
        } else {
            console.error(error)
            res.send('Failed to create a new bucket')
        }
    }
}

const detail = async (_req, res) => {
    try {
        await Axios({
            method: 'GET',
            url: 'https://developer.api.autodesk.com/oss/v2/buckets/' + encodeURIComponent(bucketKey) + '/details',
            headers: {
                Authorization: 'Bearer ' + global.access_token
            }
        })
        res.redirect('/upload.html')
    } catch (error) {
        console.log(error)
        res.send('Failed to verify the new bucket')
    }
}

const upload = async (req, res) => {
    try {
        const data = await fs.readFile(req.file.path)
        const response = await Axios({
            method: 'PUT',
            url: 'https://developer.api.autodesk.com/oss/v2/buckets/' +
                encodeURIComponent(bucketKey) + '/objects/' +
                encodeURIComponent(req.file.originalname),
            headers: {
                Authorization: 'Bearer ' + global.access_token,
                'Content-Disposition': req.file.originalname,
                'Content-Length': data.length
            },
            data: data
        })

        var urn = response.data.objectId.toBase64()
        res.redirect('/api/forge/modelderivative/' + urn)
    } catch (error) {
        console.error(error)
        res.send('Failed to create a new object in the bucket')
    }

}

module.exports = { create, detail, upload }
