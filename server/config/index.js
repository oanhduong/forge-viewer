module.exports = {
    server: {
        port: process.env.PORT || 3000,
    },
    forge: {
        clientID: process.env.FORGE_CLIENT_ID || 'UMEdcKsKiSEQCv9wnchI6bhXb5oGAJMA',
        clientSecret: process.env.FORGE_CLIENT_SECRET || 'GRURsZC95qucNUon',
        scope: 'data:read data:write data:create bucket:create bucket:read',
        policyKey: process.env.POLICY_KEY || 'transient', // Expires in 24hr
    },
    tempFolder: 'tmp',
}
